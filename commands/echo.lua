local Celeste = require 'celeste'
return function(...)
	msg = select(1, ...)
	res = string.gmatch(msg[8], "([^" .. Celeste.prefix .. " echo!" .. "]+)")
	for str in res do
		msg:reply(str)
	end
end
