local Celeste = require 'celeste'
return function(...)
  local lang = ''
  msg = select(1, ...)
  local t, i = {}, 0  -- iterate thru the msg table & extract the language,
                      -- then the rest of the message body
  for str in string.gmatch(msg[8], "([^" .. "```" .. "]+)") do
    i = i + 1
    if i == 2 then
      j = 1
      for l in str:gmatch("[^\r\n]+") do
        print(j, l)
        j = j + 1
        table.insert(t, l)
      end
    end
  end
  lang = t[1] ----- vv important do not remove it sets the language
  -- looking for system / dirty `rm -fr` commands
  for i = 1, #t do
    if t[i] ==
      string.match("rm -fr", t[i]) or string.match("rm -rf", t[i])            -- bad stuff in general
      or string.match('os.execute', t[i]) or string.match('System.cmd', t[i]) -- lua & elixir exec commands
      or string.match('exec(', t[i]) or string.match('system(', t[i])         -- python & ruby
    then   
        msg:reply("im not that easy, slut <3")
      return
    end
  end
  if Celeste.langfunctions[lang] == nil then
    msg:reply("language not supported, slut <3")
  else
    msg:reply(require(Celeste.langfunctions[lang])(t, ...))
  end
  -- log all the commands
  print('lang: ', lang, ' functions: ', Celeste.langfunctions[lang])
  local log = io.open('commands/log.json', 'w+')
  if lang == nil then 
    log:write('no lang ', 'functions: ', Celeste.langfunctions[lang])
  elseif Celeste.langfunctions[lang] == nil then
    log:write('lang: ', lang, ' no functions[lang]',"\n")
  else
    log:write('lang:', lang, 'functions: ', Celeste.langfunctions[lang],"\n")
  end 
  log:close()
end
